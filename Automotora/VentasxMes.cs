﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Automotora
{
    public partial class VentasxMes : Form
    {
        public VentasxMes()
        {
            InitializeComponent();
            cargarFechas();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            BuscarVentas();
        }


        private void BuscarVentas() {
            String fecha = Convert.ToString(cboFecha.SelectedItem);
            int totalVentas = 0;
            int cantidadVentas = 0;


            foreach (Clases.Ventas v in Program.ventas) {

                if ((v.fecha.Month + "/" + v.fecha.Year).Equals(fecha)) {
                    totalVentas += v.precio;
                    cantidadVentas++;
                
                }
            
            }

            txtTotalVentas.Text = Convert.ToString(totalVentas);
            txtUnidadesVendidas.Text = Convert.ToString(cantidadVentas);
        
        
        }




        private void cargarFechas() {

            foreach (Clases.Ventas v in Program.ventas) {
                String fecha = v.fecha.Month + "/" + v.fecha.Year;
                if(!verificarFecha(fecha)){
                     cboFecha.Items.Add(fecha);
                }
            }
            cboFecha.SelectedIndex = 0;
        }

        private Boolean verificarFecha(String fecha) {

            for (int i = 0; i < cboFecha.Items.Count;i++ ) { 
                if(cboFecha.Items[i].Equals(fecha)){
                    return true;
                }
            
            }

            return false;
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
