﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Automotora
{
    public partial class venta : Form
    {
        public venta()
        {
            InitializeComponent();
            CargarVehiculos();
            fechaCompra.Format = DateTimePickerFormat.Custom;
            fechaCompra.CustomFormat = "MM/yyyy";
            fechaCompra.Enabled = true;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!BuscarCliente()) { 
                MessageBox.Show("No existe cliente en sistema");
            }else{
                btnVender.Enabled= true;
            
            }
        }

        private Boolean BuscarCliente() { 
        
            foreach(Clases.Cliente c in Program.clientes ){
            
                if(c.rut.Equals(txtRut.Text)){
                    txtNombre.Text = c.nombre;
                    picImageCliente.ImageLocation = c.url_foto;
                    return true;
                
                }
            
            }

            return false;
        }

        private void CargarVehiculos() {

            foreach (Clases.Vehiculo v in Program.autos) {

                if(!v.vendido){
                    cboPatentes.Items.Add(v.patente);
                }
            
            }
        
        
        }

        private void BuscarVehiculo() {

            foreach (Clases.Vehiculo v in Program.autos)
            {

                if(v.patente.Equals(cboPatentes.SelectedItem )){

                    txtMarca.Text = v.Smarca;
                    txtModelo.Text = v.Smodelo;
                    txtTipo.Text = v.Stipo;
                    txtPrecio.Text = Convert.ToString(v.precio);
                    picImageAuto.ImageLocation = v.url_imagen;

                }


            }
        
        
        }

        private void cboPatentes_SelectedIndexChanged(object sender, EventArgs e)
        {
            BuscarVehiculo();
        }

        private void btnVender_Click(object sender, EventArgs e)
        {
            GenerarVenta();
        }

        private void GenerarVenta() {

            
                Clases.Ventas v = new Clases.Ventas();

                v.cliente = txtRut.Text;
                v.fecha = fechaCompra.Value;
                v.patente = Convert.ToString( cboPatentes.SelectedItem);
                v.marca = txtMarca.Text;
                v.modelo = txtModelo.Text;
                v.tipo = txtTipo.Text;
                v.precio = Convert.ToInt32( txtPrecio.Text);

                Program.ventas.Add(v);
                int i = 0;

                /*foreach (Clases.Vehiculo c in Program.autos){
                
                    if(c.patente.Equals(cboPatentes.SelectedItem )){
                        
                        c.vendido = true;
                        MessageBox.Show(Convert.ToString( i));
                        Program.autos[i] = c;
                    }

                    
                }*/

                MessageBox.Show("Venta Exitosa !!!");
                
            
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
