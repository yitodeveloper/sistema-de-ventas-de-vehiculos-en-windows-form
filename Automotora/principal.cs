﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Automotora
{
    public partial class principal : Form
    {
        public principal()
        {
            InitializeComponent();
        }

        private void clientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new clientes().Show();
        }

        private void modelosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new Mmodelos().Show();
        }

        private void venderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new venta().Show();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void modeloMasVendidoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new modeloMV().Show();
        }

        private void marcaMasVendidaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new marcaMV().Show(); 
        }

        private void totalDeVentasPorMesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new VentasxMes().Show();
        }
    }
}
