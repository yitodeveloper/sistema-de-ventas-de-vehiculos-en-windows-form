﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Automotora.Clases
{
    class Vehiculo
    {
        public String patente;
        public int marca;
        public int modelo;
        public int tipo;
        public String Smarca;
        public String Smodelo;
        public String Stipo;
        public int nro_puertas;
        public int precio;
        
        public String url_imagen;
        public Boolean vendido;

        public Vehiculo(int marca, int modelo , int nro_puertas, int precio, int tipo, String url_imagen) {

            this.marca = marca;
            this.modelo = modelo;
            this.nro_puertas = nro_puertas;
            this.precio = precio;
            this.tipo = tipo;
            this.url_imagen = url_imagen;
        
        }


        public Vehiculo() {
            vendido = false;
        }

    }
}
