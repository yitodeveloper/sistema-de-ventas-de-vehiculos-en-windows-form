﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Automotora.Clases
{
    class Cliente
    {
        public String rut;
        public String nombre;
        public String direccion;
        public String url_foto;
        public int comuna;
        public int ciudad;

        public Cliente() { }

        public Cliente(String rut, String nombre, String direccion, String url_foto, int comuna, int ciudad) {

            this.rut = rut;
            this.nombre = nombre;
            this.direccion = direccion;
            this.url_foto = url_foto;
            this.comuna = comuna;
            this.ciudad = ciudad;

        }



    }
}
