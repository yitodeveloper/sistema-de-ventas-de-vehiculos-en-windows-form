﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Automotora
{
    public partial class modeloMV : Form
    {
        public modeloMV()
        {
            InitializeComponent();
            Calcular();
        }

        private void Calcular() {
            estmodelos v = modeloMasVendido();
            txtModelo.Text = v.nombre;
            txtMarca.Text = v.marca;
            txtCantidadVentas.Text = Convert.ToString(v.cantVecesVendidas);
            txtTotalVentas.Text = "$" + Convert.ToString(v.acuventas);
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }


        public struct estmodelos{
            
                public String nombre;
                public String marca;
                public int cantVecesVendidas;
                public int acuventas;
                
         
            }



        public estmodelos modeloMasVendido(){
            
                    ArrayList auxiliar = Program.ventas;
                    ArrayList estadistica = new ArrayList();
                    estmodelos masvendida = new estmodelos();

                    foreach(Clases.Ventas v in Program.ventas )
                    {
                        estmodelos e = new estmodelos();
                        e.nombre= v.modelo;
                        e.marca = v.marca;
                            foreach (Clases.Ventas c in auxiliar) 
                            { 
                    
                                    if(v.modelo.Equals(c.modelo))
                                    {
                                        e.acuventas+= c.precio;
                                        e.cantVecesVendidas++;
                                    }
                    
                    
                             }
                        if(!ExisteEstadisticaModelo(e.nombre, estadistica)){
                            estadistica.Add(e);
                        }
                    }

                    
                foreach(estmodelos aux in estadistica ){

                    MessageBox.Show("Estadistica : Modelo : "+aux.nombre+" Cantidad Ventas : "+aux.cantVecesVendidas+" Total Ventas :"+ aux.acuventas);
                    if(aux.cantVecesVendidas>masvendida.cantVecesVendidas ){
                        masvendida = aux;
                    
                    }
            
                }

                return masvendida;
                

                    

            }
        

            private Boolean ExisteEstadisticaModelo(String nombre, ArrayList estadistica){
            
                    foreach(estmodelos e in estadistica ){
                        
                        if(e.nombre.Equals(nombre)){
                            return true;
                        
                        }
                    
                    }
            
            
                return false;
            }


        }
    }

