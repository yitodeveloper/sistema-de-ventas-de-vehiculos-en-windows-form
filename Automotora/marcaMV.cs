﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Automotora
{
    public partial class marcaMV : Form
    {
        public marcaMV()
        {
            InitializeComponent();
            Calcular();
        }

        private void Calcular()
        {
            estmarcas v = marcaMasVendida();
            txtMarca.Text = v.nombre;
            txtCantidadVentas.Text = Convert.ToString(v.cantVecesVendidas);
            txtTotalVentas.Text = "$" + Convert.ToString(v.acuventas);
        }



        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        public struct estmarcas        {

            public String nombre;
            public int cantVecesVendidas;
            public int acuventas;


        }



        public estmarcas marcaMasVendida()
        {

            ArrayList auxiliar = Program.ventas;
            ArrayList estadistica = new ArrayList();
            estmarcas masvendida = new estmarcas();

            foreach (Clases.Ventas v in Program.ventas)
            {
                estmarcas e = new estmarcas();
                e.nombre = v.marca;
                foreach (Clases.Ventas c in auxiliar)
                {

                    if (v.marca.Equals(c.marca))
                    {
                        e.acuventas += c.precio;
                        e.cantVecesVendidas++;
                    }


                }
                if (!ExisteEstadisticaMarca(e.nombre, estadistica))
                {
                    estadistica.Add(e);
                }
            }


            foreach (estmarcas aux in estadistica)
            {

                MessageBox.Show("Estadistica : Marca : " + aux.nombre + " Cantidad Ventas : " + aux.cantVecesVendidas + " Total Ventas :" + aux.acuventas);
                if (aux.cantVecesVendidas > masvendida.cantVecesVendidas)
                {
                    masvendida = aux;

                }

            }

            return masvendida;




        }


        private Boolean ExisteEstadisticaMarca(String nombre, ArrayList estadistica)
        {

            foreach (estmarcas e in estadistica)
            {

                if (e.nombre.Equals(nombre))
                {
                    return true;

                }

            }


            return false;
        }
    }
}
