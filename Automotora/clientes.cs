﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Automotora
{
    public partial class clientes : Form
    {
        public String ruta;
        public clientes()
        {
            InitializeComponent();
            CargarComunasyCiudades();
            CargarClientes();
            cboCiudad.SelectedIndex = 1;
            cboComuna.SelectedIndex = 1;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (AddCliente())
            {
                MessageBox.Show("Se agrego Correctamente a Cliente");
                LimpiarFormulario();

            }
            else {

                MessageBox.Show("El cliente ya existe");
            }
        }

        public Boolean AddCliente() {
            if (!ExisteCliente())
            {
                Clases.Cliente c = new Clases.Cliente();
                c.rut = txtRut.Text;
                c.nombre = txtNombre.Text;
                c.direccion = txtDireccion.Text;
                c.url_foto = ruta;
                c.comuna = cboComuna.SelectedIndex;
                c.ciudad = cboCiudad.SelectedIndex;

                Program.clientes.Add(c);
                lstboListaClientes.Items.Add(c.rut);

                return true;
            }
            else {
                
                return false;
            }
            

        }

        public Boolean ExisteCliente() {
            foreach (Clases.Cliente c in Program.clientes) { 
            
                if(c.rut.Equals(txtRut.Text)){
                    return true;
                
                }
            
            }

            return false;
                
        }

        public void LimpiarFormulario() {

            txtRut.Text = "";
            txtNombre.Text = "";
            txtDireccion.Text = "";
            picboxFoto.ImageLocation = null;

        
        }

        public void EliminarCliente() {
            if (lstboListaClientes.SelectedIndex >= 0)
            {
                Program.clientes.RemoveAt(lstboListaClientes.SelectedIndex);
                lstboListaClientes.Items.RemoveAt(lstboListaClientes.SelectedIndex);
                
                MessageBox.Show("Cliente se ha eliminado correctamente");
                LimpiarFormulario();
            }
            else {

                MessageBox.Show("Debe Seleccionar un Cliente a eliminar");
            
            }
        }


        public void ModificaCliente()
        {
            if (lstboListaClientes.SelectedIndex >= 0)
            {
                Clases.Cliente c = new Clases.Cliente();
                c.rut = txtRut.Text;
                c.nombre = txtNombre.Text;
                c.direccion = txtDireccion.Text;
                c.url_foto = ruta;
                c.comuna = cboComuna.SelectedIndex;
                c.ciudad = cboCiudad.SelectedIndex;
                lstboListaClientes.Items[lstboListaClientes.SelectedIndex] = c.rut;
                Program.clientes[lstboListaClientes.SelectedIndex] = c;
                MessageBox.Show("Se ha modificado los datos satisfactoriamente");
            }
            else {
                MessageBox.Show("No existe ningun registro de Clientes");
            }

        }

        public void BuscarSeleccionado() {

            foreach (Clases.Cliente c in Program.clientes) {

                if (c.rut.Equals(lstboListaClientes.SelectedItem)) {

                    txtRut.Text = c.rut;
                    txtNombre.Text = c.nombre;
                    txtDireccion.Text = c.direccion;

                    picboxFoto.ImageLocation = c.url_foto;

                    ruta = c.url_foto;

                    cboComuna.SelectedIndex = c.comuna;
                    cboCiudad.SelectedIndex = c.ciudad;

                    
                }
            
            
            }
        
        
        
        }



        public void CargarClientes() {

            foreach (Clases.Cliente c in Program.clientes) {

                lstboListaClientes.Items.Add(c.rut);
            
            
            }
        
        
        }


        public void CargarComunasyCiudades() {
            // carga de ciudades
            cboCiudad.Items.Add("Arica");
            cboCiudad.Items.Add("Iquique");
            cboCiudad.Items.Add("Calama");
            cboCiudad.Items.Add("San Pedro de Atacama");
            cboCiudad.Items.Add("Antofagasta");
            cboCiudad.Items.Add("Copiapo");
            cboCiudad.Items.Add("Vallenar");
            cboCiudad.Items.Add("La serena");
            cboCiudad.Items.Add("Coquimbo");
            cboCiudad.Items.Add("Ovalle");
            cboCiudad.Items.Add("Los vilos");
            cboCiudad.Items.Add("Viña del Mar");
            cboCiudad.Items.Add("Valparaiso");
            cboCiudad.Items.Add("Santiago");
            cboCiudad.Items.Add("Rancagua");
            cboCiudad.Items.Add("Talca");
            cboCiudad.Items.Add("Chillan");
            cboCiudad.Items.Add("Valdivia");
            cboCiudad.Items.Add("Concepción");
            cboCiudad.Items.Add("Osorno");
            cboCiudad.Items.Add("Los Angeles");
            cboCiudad.Items.Add("Pto. Montt");
            cboCiudad.Items.Add("Puerto Varas");
            cboCiudad.Items.Add("Pta. Arenas");
            // carga de comunas
            cboComuna.Items.Add("Arica");
            cboComuna.Items.Add("Iquique");
            cboComuna.Items.Add("Calama");
            cboComuna.Items.Add("San Pedro de Atacama");
            cboComuna.Items.Add("Antofagasta");
            cboComuna.Items.Add("Copiapo");
            cboComuna.Items.Add("Vallenar");
            cboComuna.Items.Add("La serena");
            cboComuna.Items.Add("Coquimbo");
            cboComuna.Items.Add("Ovalle");
            cboComuna.Items.Add("Los vilos");
            cboComuna.Items.Add("Viña del Mar");
            cboComuna.Items.Add("Valparaiso");
            cboComuna.Items.Add("Cerrillos");
            cboComuna.Items.Add("Cerro Navia");
            cboComuna.Items.Add("Conchalí");
            cboComuna.Items.Add("El bosque");
            cboComuna.Items.Add("Estacion Central");
            cboComuna.Items.Add("Huechuraba");
            cboComuna.Items.Add("Independencia");
            cboComuna.Items.Add("La Cisterna");
            cboComuna.Items.Add("La florida");
            cboComuna.Items.Add("La Pintana");
            cboComuna.Items.Add("La Granja");
            cboComuna.Items.Add("La Reina");
            cboComuna.Items.Add("Las Condes");
            cboComuna.Items.Add("Lo Barnechea");
            cboComuna.Items.Add("Lo Espejo");
            cboComuna.Items.Add("Lo Prado");
            cboComuna.Items.Add("Macul");
            cboComuna.Items.Add("Maipu");
            cboComuna.Items.Add("Ñuñoa");
            cboComuna.Items.Add("Pedro Aguirre Cerda");
            cboComuna.Items.Add("Peñanolen");
            cboComuna.Items.Add("Providencia");
            cboComuna.Items.Add("Pudahuel");
            cboComuna.Items.Add("Quilicura");
            cboComuna.Items.Add("Quinta Normal");
            cboComuna.Items.Add("Recoleta");
            cboComuna.Items.Add("Renca");
            cboComuna.Items.Add("San Miguel");
            cboComuna.Items.Add("San Joaquin");
            cboComuna.Items.Add("San Ramon");
            cboComuna.Items.Add("Vitacura");
            cboComuna.Items.Add("Santiago");
            cboComuna.Items.Add("Rancagua");
            cboComuna.Items.Add("Talca");
            cboComuna.Items.Add("Chillan");
            cboComuna.Items.Add("Valdivia");
            cboComuna.Items.Add("Concepción");
            cboComuna.Items.Add("Osorno");
            cboComuna.Items.Add("Los Angeles");
            cboComuna.Items.Add("Pto. Montt");
            cboComuna.Items.Add("Puerto Varas");
            cboComuna.Items.Add("Pta. Arenas");
        
        
        }

        public void checkFormulario() {

            if (txtRut.Text.Length > 0 && txtNombre.Text.Length > 0 && txtDireccion.Text.Length > 0)
            {
                btnAñadir.Enabled = true;

            }
            else {

                btnAñadir.Enabled = false;
            
            }
        
        
        
        }

        private void txtRut_Leave(object sender, EventArgs e)
        {
            checkFormulario();
        }

        private void txtNombre_Leave(object sender, EventArgs e)
        {
            checkFormulario();
        }

        private void txtDireccion_Leave(object sender, EventArgs e)
        {
            checkFormulario();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnAddFoto_Click(object sender, EventArgs e)
        {
            OpenFileDialog o = new OpenFileDialog();
            o.Filter = "Archivos de Imagenes|*.jpg;*.png;*.jpeg;*.PNG";
            if (o.ShowDialog() == DialogResult.OK) {
                ruta = o.FileName;
                
                picboxFoto.ImageLocation = ruta;
            
            }
        }

        private void lstboListaClientes_MouseClick(object sender, MouseEventArgs e)
        {
            BuscarSeleccionado();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            ModificaCliente();
            
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            EliminarCliente();
        }
    }
}
