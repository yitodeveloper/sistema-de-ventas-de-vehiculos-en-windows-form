﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Automotora
{
    public partial class Mmodelos : Form
    {

        public String ruta="";
        public Mmodelos()
        {
            InitializeComponent();
            inicializarComboBox();
            CargarLista();
            textBox1.Text = "Sin información";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (ADDVehiculo())
            {
                MessageBox.Show("Se agrego correctamente vehiculo");

            }
            else {
                MessageBox.Show("Vehiculo ya existe en el registro");
            
            }
        }
        private void inicializarComboBox() {
            //Cargando Tipos
            cboTipo.Items.Add("Sport");
            cboTipo.Items.Add("CityCar");
            cboTipo.Items.Add("Sedan");
            cboTipo.Items.Add("4x4");
            cboTipo.Items.Add("Moto");
            //Cargando Marcas
            cboMarca.Items.Add("Abarth");
            cboMarca.Items.Add("Alfa Romeo");
            cboMarca.Items.Add("Aro");
            cboMarca.Items.Add("Asia");
            cboMarca.Items.Add("Asia Motors");
            cboMarca.Items.Add("Aston Martin");
            cboMarca.Items.Add("Audi");
            cboMarca.Items.Add("Austin");
            cboMarca.Items.Add("Auverland");
            cboMarca.Items.Add("Bentley");
            cboMarca.Items.Add("Bertone");
            cboMarca.Items.Add("BMW");
            cboMarca.Items.Add("Cadillac");
            cboMarca.Items.Add("Chevrolet");
            cboMarca.Items.Add("Citroen");
            cboMarca.Items.Add("Corsa");
            cboMarca.Items.Add("Corvette");
            cboMarca.Items.Add("Dacia");
            cboMarca.Items.Add("Daewoo");
            cboMarca.Items.Add("Ferrari");
            cboMarca.Items.Add("Fiat");
            cboMarca.Items.Add("General Motors");
            cboMarca.Items.Add("Hyundai");
            cboMarca.Items.Add("Kia");
            cboMarca.Items.Add("Mahindra");
            cboMarca.Items.Add("Nissan");
            cboMarca.Items.Add("Opel");
            cboMarca.Items.Add("Susuki");
            cboMarca.Items.Add("Toyota");
            //cargar modelos
            cboModelo.Items.Add("Aston Martin Cygnet");
            cboModelo.Items.Add("Chery QQ");
            cboModelo.Items.Add("Chevrolet Spark");
            cboModelo.Items.Add("Citroën C1");
            cboModelo.Items.Add("Citroën C-Zero (Eléctrico)");
            cboModelo.Items.Add("Fiat 500");
            cboModelo.Items.Add("Fiat Panda");
            cboModelo.Items.Add("Ford Ka");
            cboModelo.Items.Add("Hyundai i10");
            cboModelo.Items.Add("Kia Picanto");
            cboModelo.Items.Add("Mitsubishi i MiEV (Eléctrico)");
            cboModelo.Items.Add("Nissan Pixo");
            cboModelo.Items.Add("Opel Adam");
            cboModelo.Items.Add("Opel Agila");
            cboModelo.Items.Add("Peugeot 108");
            cboModelo.Items.Add("Peugeot iOn (Eléctrico)");
            cboModelo.Items.Add("Renault Twingo");
            cboModelo.Items.Add("SEAT Mii");
            cboModelo.Items.Add("Škoda Citigo");
            cboModelo.Items.Add("Smart Fortwo");
            cboModelo.Items.Add("Suzuki Alto");
            cboModelo.Items.Add("Suzuki Splash");
            cboModelo.Items.Add("Tata Indica Vista");
            cboModelo.Items.Add("Tata Nano");
            cboModelo.Items.Add("Toyota Aygo");
            cboModelo.Items.Add("Toyota iQ");
            cboModelo.Items.Add("Volkswagen up!");
            cboModelo.Items.Add("Alfa Romeo MiTo");
            cboModelo.Items.Add("Audi A1");
            cboModelo.Items.Add("BMW i3 (Eléctrico)");
            cboModelo.Items.Add("Chevrolet Aveo");
            cboModelo.Items.Add("Citroën C3");
            cboModelo.Items.Add("Citroën DS3");
            cboModelo.Items.Add("Dacia Logan");
            cboModelo.Items.Add("Dacia Sandero");
            cboModelo.Items.Add("Daihatsu Sirion");
            cboModelo.Items.Add("Fiat Palio");
            cboModelo.Items.Add("Fiat Punto");
            cboModelo.Items.Add("Fiat Uno");
            cboModelo.Items.Add("Ford Fiesta");
            cboModelo.Items.Add("Honda City");
            cboModelo.Items.Add("Hyundai Accent");
            cboModelo.Items.Add("Hyundai i20");
            cboModelo.Items.Add("Kia Rio");
            cboModelo.Items.Add("Lada Granta");
            cboModelo.Items.Add("Lancia Ypsilon");
            cboModelo.Items.Add("Mazda 2");
            cboModelo.Items.Add("MINI Hatch");
            cboModelo.Items.Add("Hyundai Elantra");
            cboModelo.Items.Add("Hyundai i30");
            cboModelo.Items.Add("Kia Cee'd");
            cboModelo.Items.Add("Kia Cerato/Forte/K3");
            cboModelo.Items.Add("Lada Priora");
            cboModelo.Items.Add("Lancia Delta (2008)");
            cboModelo.Items.Add("Lexus CT");
            cboModelo.Items.Add("Mazda 3");
            cboModelo.Items.Add("Mercedes-Benz Clase A");
            cboModelo.Items.Add("Mercedes-Benz Clase CLA");
            cboModelo.Items.Add("Mitsubishi Lancer");
            cboModelo.Items.Add("Nissan Leaf (Eléctrico)");
            cboModelo.Items.Add("Nissan Sentra");
            cboModelo.Items.Add("Nissan Tiida");
            cboModelo.Items.Add("Opel Ampera (Eléctrico*)");
            cboModelo.Items.Add("Opel Astra");
            cboModelo.Items.Add("Peugeot 301 (2012)");
            cboModelo.Items.Add("Peugeot 308");
            cboModelo.Items.Add("Peugeot 408");
            cboModelo.Items.Add("Qoros 3");
            cboModelo.Items.Add("Ford Taurus");
            cboModelo.Items.Add("Honda Legend");
            cboModelo.Items.Add("Holden Commodore");
            cboModelo.Items.Add("Hyundai Genesis");
            cboModelo.Items.Add("Hyundai Grandeur");
            cboModelo.Items.Add("Infiniti M");
            cboModelo.Items.Add("Jaguar XF");
            cboModelo.Items.Add("Kia Cadenza/K7");
            cboModelo.Items.Add("Lancia Thema (2011)");
            cboModelo.Items.Add("Lexus GS");
            cboModelo.Items.Add("Lincoln MKS");
            cboModelo.Items.Add("Peugeot 405");
            cboModelo.Items.Add("Peugeot 406");
            cboModelo.Items.Add("Peugeot 407");
            cboModelo.Items.Add("Pontiac G6");
            cboModelo.Items.Add("Renault 12");
            cboModelo.Items.Add("Renault 18");
            cboModelo.Items.Add("Renault 21");
            cboModelo.Items.Add("Saab 9-3");
            cboModelo.Items.Add("SEAT Exeo");
            cboModelo.Items.Add("SEAT 132/Fiat 132");
            cboModelo.Items.Add("Toyota Carina E/Corona");
            cboModelo.Items.Add("Volkswagen Santana");

            cboMarca.SelectedIndex = 1;
            cboModelo.SelectedIndex = 1;
            cboTipo.SelectedIndex = 1;
        
        }

        


        private Boolean ADDVehiculo() {

            Clases.Vehiculo v = new Clases.Vehiculo();
            v.patente = txtPatente.Text;

            if (!ExisteVehiculo(v.patente))
            {

                v.marca = cboMarca.SelectedIndex;
                v.Smarca = Convert.ToString( cboMarca.SelectedItem);
                v.modelo = cboModelo.SelectedIndex;
                v.nro_puertas = Convert.ToInt32( txtNroPuertas.Text);
                v.Smodelo = Convert.ToString(cboModelo.SelectedItem);
                v.precio = Convert.ToInt32( txtPrecio.Text);
                v.Stipo = Convert.ToString(cboTipo.SelectedItem);
                v.tipo = cboTipo.SelectedIndex;
                v.url_imagen = ruta;
                Program.autos.Add(v);
                lstLista.Items.Add(v.patente);
                LimpiarFormulario();
                return true;
            }
            else{
                return false;
            
            }

        }


        public void BuscarSeleccionado()
        {

            foreach (Clases.Vehiculo c in Program.autos)
            {

                if (c.patente.Equals(lstLista.SelectedItem))
                {

                    txtPatente.Text = c.patente;
                    txtNroPuertas.Text = Convert.ToString( c.nro_puertas);
                    txtPrecio.Text = Convert.ToString(c.precio);
                    cboMarca.SelectedIndex = c.marca;
                    cboModelo.SelectedIndex = c.modelo;
                    cboTipo.SelectedIndex = c.tipo;
                    ruta = c.url_imagen;
                    picboImagen.ImageLocation=c.url_imagen;
                    if (c.vendido)
                    {
                        textBox1.Text = "Vendido";

                    }
                    else {
                    
                    
                    }

                        textBox1.Text = "Disponible para venta";

                    }


            }



        }

        private Boolean ExisteVehiculo(String cpatente){
        
            foreach( Clases.Vehiculo v in Program.autos){
                
                if(v.patente.Equals(cpatente)){
                    return true;
                }
                
            }
        
            return false;
        }

        private void btnCargarImagen_Click(object sender, EventArgs e)
        {
            OpenFileDialog o = new OpenFileDialog();
            o.Filter = "";
            if(o.ShowDialog() == DialogResult.OK){
                picboImagen.ImageLocation = o.FileName;
                ruta = o.FileName;
            }
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if (txtPatente.Text.Length > 0)
            {
                ModificarVehiculo();

            }
            else {

                MessageBox.Show("Patente no puede estar en blanco");
            }
            

        }

        private void ModificarVehiculo() {

            if (lstLista.SelectedIndex >= 0)
            {
                Clases.Vehiculo v = new Clases.Vehiculo();
                v.patente = txtPatente.Text;
                v.marca = cboMarca.SelectedIndex;
                v.Smarca = Convert.ToString(cboMarca.SelectedItem);
                v.modelo = cboModelo.SelectedIndex;
                v.nro_puertas = Convert.ToInt32(txtNroPuertas.Text);
                v.Smodelo = Convert.ToString(cboModelo.SelectedItem);
                v.precio = Convert.ToInt32(txtPrecio.Text);
                v.Stipo = Convert.ToString(cboTipo.SelectedItem);
                v.tipo = cboTipo.SelectedIndex;
                v.url_imagen = ruta;
                Program.autos[lstLista.SelectedIndex] = v;
                lstLista.Items[lstLista.SelectedIndex] = v.patente;
                MessageBox.Show("El vehiculo "+cboModelo.SelectedItem+" patente nº : "+v.patente+" fue modificado correctamente");
            }
            else {

                MessageBox.Show("Debe seleccionar Vehiculo de la lista");
            
            }
            
        
        }


        private void EliminarVehiculo() {

            if (lstLista.SelectedIndex >= 0)
            {

                Program.autos.RemoveAt(lstLista.SelectedIndex);
                lstLista.Items.RemoveAt(lstLista.SelectedIndex);
                LimpiarFormulario();
                MessageBox.Show("Vehiculo eliminado Satisfactoriamente");

            }
            else {

                MessageBox.Show("Debe seleccionar Vehiculo de la lista");
                LimpiarFormulario();
            }
        
        
        }

        private void LimpiarFormulario() {
            txtPatente.Text = "";
            txtNroPuertas.Text = "";
            txtPrecio.Text = "";

            picboImagen.ImageLocation = "";
            ruta = "";
        
        
        
        }

        private void CargarLista() {


            foreach (Clases.Vehiculo v in Program.autos) {

                lstLista.Items.Add(v.patente);
            
            }
        
        
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            EliminarVehiculo();
        }

        private void lstLista_MouseClick(object sender, MouseEventArgs e)
        {
            BuscarSeleccionado();
        }

    }
}
